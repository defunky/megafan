# Megafan

A Megaman inspired game built in Java for 1st year. 
- 5 Levels including 2 boss fights and 1 runner stage.
- Unique Enemies
- Old school megaman music
- Highscore ladder.

### Dependencies
JBox2D wrapper, found in lib folder