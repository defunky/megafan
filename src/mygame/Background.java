/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.awt.*;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 * This class is responsible for the different parallax scrolling background
 * to different levels in the game.
 * It is also responsible in display the players score and lives.
 * @author Abdalrahmane
 */

public class Background extends UserView {
    
    private BufferedImage bg,bg2,bg3,bg4,bg5,l1;
    private Player player;
    private MyGame game;
    private GameLevel w;
    
    /**
     * Constructor of the method
     * @param w the current world on
     * @param width width of the game window
     * @param height height of the game window
     * @param player the current player
     * @param game the actual game
     */

    public Background(GameLevel w, int width, int height, Player player, MyGame game) {
        super(w, width, height);
        this.player = player;
        this.game = game;
        this.w = w;
    }
    
    /**
     * Finds the background images and saves them to variable to be accessed 
     * in paintBackground method
     */
    public void newBG(){
         try {
            bg = ImageIO.read(new File("data/misc/bg.gif"));
            bg2 = ImageIO.read(new File("data/misc/bg2.gif"));
            bg3 = ImageIO.read(new File("data/misc/lvl2bg.gif"));
            bg4 = ImageIO.read(new File("data/misc/lvl2bg2.gif"));
            bg5 = ImageIO.read(new File("data/misc/lvl3bg.gif"));
            l1 = ImageIO.read(new File("data/misc/live.gif"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * It will display the two layer backgrounds
     * and will use the players x-coord to scroll the images accordingly creating
     * a sense of depth in the game.
     * @param g Graphics2D
     */
    @Override
    public void paintBackground(Graphics2D g){
        super.paintBackground(g); 
        if(game.returnLevel() == 0){
            g.drawImage(bg, (-(int)player.getPosition().x-20), -100, this);
            g.drawImage(bg2, (-(int)player.getPosition().x-20)*2, -100, this);                    
        }else if(game.returnLevel() == 1 || game.returnLevel() == 3){
            g.drawImage(bg3, (-(int)player.getPosition().x-20), -100, this);
            g.drawImage(bg4, (-(int)player.getPosition().x-20)*2, -100, this);
        }else{
            g.drawImage(bg5, (-(int)player.getPosition().x-20), -100, this);
        }
    }
    /**
     * Draws the players score,lives and the current level target score accordingly, 
     * it will also display a message
     * if the player has no more lives remaining.
     * @param g Graphics2D
     */
    @Override
    public void paintForeground(Graphics2D g){
        super.paintForeground(g);
        ((Graphics)g).setColor(Color.white);
        g.setFont(new Font("default", Font.BOLD, 16));
        g.drawString("Target: " +w.getTarget(), 360,25);
        g.drawString("Score: "+player.getScore(), 705, 25);
        for(int i = 0; i < player.getLives(); i++){
            g.drawImage(l1, 20+20*i, 20, this);
        }
        if(player.getLives() <= 0){
            g.drawString("GAME OVER, PRESS RESTART TO TRY AGAIN!",220,325);
        }
    }
    
    /**
     * Called when a new level has started, allowing the player variable to update
     * accordingly keeping making parallax scrolling work on every level
     * @param p the new player
     */
    public void setBody(Player p){
        this.player = p;
    }
    /**
     * This method is used to update the current world level. It's used to keep the target score
     * updated throughout each level.
     * @param w the new level
     */
    public void sW(GameLevel w){
        this.w = w;
    }
    
}
