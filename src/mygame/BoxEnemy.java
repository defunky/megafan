/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * BoxEnemy is an enemy which consist of 4 equal box fixtures
 * This allows unique enemy as the only way to damage it is through shooting
 * it through faceFixture otherwise the bullets will bounce off.
 * @author Abdalrahmane
 */
public class BoxEnemy extends Enemy {
    private static final float BOX_WIDTH = 1;
    private static final float BOX_HEIGHT = 1;
    private static final float BOX_DIV = 1.5f;
    private static final int WALKING_SPEED = 2;
    private static final int RANGE_DISTANCE = 10;
    private static final int JUMPING_SPEED = 0;
    private static final int HEALTH = 3;
    private static SolidFixture headFixture, bodyFixture, footFixture;
    private static Player player;
    private SolidFixture faceFixture;
    private BodyImage bodyImage;
    private Vec2 offset = new Vec2(0,-1.2f); //image offset
    /** 
     * The Constructor of box enemy, creates the fixtures and sets the amount of health
     * walking speed, jump speed and range distance
     * <p>Walking Speed = 2</p>
     * <p>Jumping Speed = 0</p>
     * <p>Health = 3</p>
     * <p>Range Distance = 10 </p>
     * @param world the current world
     * @param player the current player
     */
    public BoxEnemy(World world, Player player) {
        super(world, player);
        setHealth(HEALTH);
        setWalkingSpeed(WALKING_SPEED);
        setJumpSpeed(JUMPING_SPEED);
        setRangeDistance(RANGE_DISTANCE);
        //enemy is composed of 4 fixture blocks: head, face, body and foot.
        Shape headShape = new BoxShape(BOX_WIDTH / BOX_DIV, BOX_HEIGHT /BOX_DIV, new Vec2(0, BOX_HEIGHT/BOX_DIV));
        headFixture = new SolidFixture(this, headShape, 10);
        
        Shape faceShape = new BoxShape(BOX_WIDTH / BOX_DIV, BOX_HEIGHT /BOX_DIV, new Vec2(0, -BOX_HEIGHT/BOX_DIV));
        faceFixture = new SolidFixture(this, faceShape, 20);
        
        Shape bodyShape = new BoxShape(BOX_WIDTH / BOX_DIV, BOX_HEIGHT /BOX_DIV, new Vec2(0,-BOX_HEIGHT *2f));
        bodyFixture = new SolidFixture(this, bodyShape, 30);
        
        Shape footShape = new BoxShape(BOX_WIDTH / BOX_DIV, BOX_HEIGHT /BOX_DIV, new Vec2(0,-BOX_HEIGHT *3.3f));
        footFixture = new SolidFixture(this, footShape, 40);
        
        bodyImage = new BodyImage("data/boxenemy/boxenemyidle.gif",5.5f);
        //offset the image by -1 to match the 4 boxshapes
        addImage(bodyImage, offset);
        //setPosition(new Vec2(7,-8));
    }  

    @Override
    public SolidFixture getFixture() {
        return faceFixture;
    }

    @Override
    public void startPosition(Vec2 startPos) {
        setPosition(startPos);
    }
    
    @Override
    public void animate(String currentState) {
        removeImage(bodyImage);
        switch (currentState) {
            case "right":
                bodyImage = new BodyImage("data/boxenemy/boxenemywalkright.gif",5.5f);
                break;
            case "left":
                bodyImage = new BodyImage("data/boxenemy/boxenemywalk.gif",5.5f);
                break;
            case "idle":
                bodyImage = new BodyImage("data/boxenemy/boxenemyidle.gif",5.5f);
                break;
        }
        addImage(bodyImage, offset);
    }

    
}
