/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Creates the bullet which is used to damage and kill enemies
 * @author Abdalrahmane
 */
public class Bullet extends DynamicBody{
    private static BodyImage bodyImage;
    private Player player;
    private static SoundClip gameMusic;
    
    /**
     * It checks which side the player is current facing to shoot bullet accordingly
     * there is also a slight offset to where bullet is shot from to give a more 
     * comfortable control feel. The bullet also disregards gravity as it shoots 
     * straight ahead, it will also play a sound when the player shoots.
     * <p> The class also sets setBullet method (inherited from dynamic bodies) to true 
     * as result setting the bullet attribute to true turns on continuous collision detection between this body 
     * and other dynamic bodies, which is considerably more expensive. It is usually only necessary for small, 
     * fast-moving objects (like bullets).</p>
     * @param world the current level it is on
     * @param player the current player
     * @param characterSide which side the player is current looking
     */

    public Bullet(World world, Player player, boolean characterSide) {
        super(world);
        this.player = player;
        setBullet(true);
        Shape bulletShape = new CircleShape(0.25f);
        SolidFixture fixture = new SolidFixture(this, bulletShape);
        float x = player.getPosition().x+2;
        float x2 = player.getPosition().x-2;
        float y = player.getPosition().y;
        /* to keep the bullet relative to the player 
          *eg. right shoots bullet rightwards */
        if(characterSide == true){
            setPosition(new Vec2(x,y));
            applyForce(new Vec2(250,0));
        }else{
            setPosition(new Vec2(x2,y));
            applyForce(new Vec2(-250,0));
        }
        setGravityScale(0);
        
        try {
            gameMusic = new SoundClip("data/music/shoot.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
}