/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * CollisionListener for the bullet to allow player to damage and kill enemies
 * @author Abdalrahmane
 */
public class BulletListener implements CollisionListener {
    private Player player;
    private static SoundClip gameMusic;
    /**
     * initializes the player 
     * @param player current player
     */
    BulletListener(Player player){
        this.player = player;
    }
    /**
     * The bullet will check if it collides with any instance that inherits
     * Abstract enemy class if so it will then check which fixture has been collided
     * with, if it's hitbox fixture it will damage or kill the enemy depending on its current life
     * however if it hits fixture that is not hitbox then it will simple bounce off.
     * The Bullet will also calculate at which direction it's been shot from
     * allowing it to bounce in the opposite direction of its original position.
     * 
     *<p> If the will also check if it collides with player, portal or another bullet
     * if so it will simply destroy itself else if it collides with anything else 
     * such as wall it will simply just bounce off.</p>
     * 
     * <p>A sound will also play depending on what as happened, eg. successful attack or bullet bounce</p>
     * 
     * @param e CollisionEvent, in this case the bullet projectile
     */
    @Override
    public void collide(CollisionEvent e) {
        if(Enemy.class.isInstance(e.getOtherBody())){ //if its an enemy
            if(e.getOtherFixture() == ((Enemy)e.getOtherBody()).getFixture()){
                e.getReportingBody().destroy();
                ((Enemy)e.getOtherBody()).hurt();
            }else{
                soundFX();
                if(player.getPosition().x > e.getOtherBody().getPosition().x){
                    ((Bullet)e.getSource()).applyForce(new Vec2(250,90));
                }else{
                    ((Bullet)e.getSource()).applyForce(new Vec2(-250,90));
                }
            }
        }else if(Player.class.isInstance(e.getOtherBody()) || Portal.class.isInstance(e.getOtherBody()) || Bullet.class.isInstance(e.getOtherBody())){
            e.getReportingBody().destroy();
        }else{
            soundFX();
            if(player.getPosition().x > e.getOtherBody().getPosition().x){
                ((Bullet)e.getSource()).applyForce(new Vec2(250,90));
            }else{
                ((Bullet)e.getSource()).applyForce(new Vec2(-250,90));
            }
        }
    }
    /**
     * This method is called when the bullet hits a wall. Plays a bounce.wav.
     */
    public void soundFX(){
        try {
            gameMusic = new SoundClip("data/music/bounce.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
}
