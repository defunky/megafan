/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 *
 * The camera will follow the player throughout the level allowing side-scrolling action
 */
public class CameraFollow implements StepListener {
    private WorldView view;
    private Body body;

    /**
     * Initializes the worldview and the current player
     * @param view The current worldview
     * @param body The current player
     */
    public CameraFollow(WorldView view, Body body) {
        this.view = view;
        this.body = body;
    }

    /**
     * This method is not used.
     * @param e StepEvent
     */
    @Override
    public void preStep(StepEvent e) {
    }
    
    /**
     * zoom out to give higher Field of View and follows the players x-coord
     * while keeping the y-coord center of the screen for more comfortable
     * feel
     * @param e StepEvent
     */
    @Override
    public void postStep(StepEvent e){
        
        float x = body.getPosition().x;
        //float y = (body.getPosition().y) + 9;
        view.setZoom(18.5f);
        view.setCentre(new Vec2(x,view.getCentre().y));
        
    }
}
