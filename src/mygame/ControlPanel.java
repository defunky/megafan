/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.DynamicBody;
import city.cs.engine.StaticBody;
import city.cs.engine.World;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Creates a control panel that is placed under the games window
 * allows start/pausing/exiting and restarting.
 * This class inherits JPanel to place all the buttons onto, and implements ActionListener
 * to allow the buttons to work.
 * @author Abdalrahmane
 */
public class ControlPanel extends JPanel implements ActionListener{
    private JButton exitBtn,startBtn,pauseBtn,restartBtn;
    private World world;
    private MyGame game;
    
    /**
     * Creates the buttons on the control panel, and adds ActionListener to them.
     * Each button is also setFocusable to false to keep the focus on the game rather the buttons
     * @param world the current world
     * @param game main game class
     */
    public ControlPanel(World world, MyGame game){    
        this.world = world;
        this.game = game;
        exitBtn = new JButton("Exit");
        startBtn = new JButton("Start");
        pauseBtn = new JButton("Pause");
        restartBtn = new JButton("Restart");
        add(startBtn);
        add(exitBtn);
        add(pauseBtn);
        add(restartBtn);
        exitBtn.addActionListener(this);
        startBtn.addActionListener(this);
        pauseBtn.addActionListener(this);
        restartBtn.addActionListener(this);
        startBtn.setFocusable(false);  
        pauseBtn.setFocusable(false);    
        restartBtn.setFocusable(false);  
    }
    
    /**
     * Called when level changes to allow control panel to work on all stages
     * @param world The current world being played
     */
    public void setWorld(World world){
        this.world = world;
    }

    /**
     * ActionListener called when user presses button on the panel
     * allows player to Start game, Pause the game, Restart the game and Exit the game
     * When the player starts or stops timerChange is called.
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
            if(e.getSource() == exitBtn){
                System.exit(0);
            }else if(e.getSource() == startBtn){
                world.start();
                timerChange(true);
            }else if(e.getSource() == pauseBtn){
                world.stop();
                timerChange(false);
            }else if(e.getSource() == restartBtn){
                game.restartLevel();
            }
    }
    /**
     * Loops through the Dynamic bodies to find instances of DragonBoss
     * and calls pause or start  timer, as timer continues counting down even if
     * the simulation is paused.
     * @param start whether the game is paused or started
     */
    public void timerChange(boolean start){
        List<DynamicBody> list = ((World)world).getDynamicBodies();
        for (DynamicBody bodies : list) {
          if (bodies instanceof TimerEnemy) {
            TimerEnemy te = (TimerEnemy) bodies;
            if(start != true){
                te.pauseTimer();
            }else{
                te.startTimer();
            }
          }
        } 
    }
}
