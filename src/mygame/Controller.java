package mygame;

/**
 * The controller used to control the player within the game
 * @author Abdalrahmane
 */
import city.cs.engine.*;
import org.jbox2d.common.Vec2;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;


/**
 * Key handler to control an Walker.
 */
public class Controller extends KeyAdapter {
    private static final float JUMPING_SPEED = 18;
    private static final float WALKING_SPEED = 6;
    private Player body; 
    private GameLevel world;
    private boolean right = true; //used to know which direction bullets should be
    
    public Controller(Player body, GameLevel world) {
        this.body = body;
        this.world = world;
    }
    
    /**
     * Called when level changes to allow control player on new stages
     * @param p current player
     * @param world current level
     */
    public void setBody(Player p, GameLevel world){
        this.body = p;
        this.world = world;
    }
    
    /**
     * Handle key press events for walking and jumping.
     * It also calls animation method within player to animate the player
     * depending on the button pressed. 
     * <p>It will also set change the boolean value of
     * 'right' depending on which button was last pressed, used to determine which 
     * direction the bullet should be shot at.</p>
     * @param e description of the key event
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_SPACE) {
            Vec2 v = body.getLinearVelocity();
            // only jump if body is not already jumping
            if (Math.abs(v.y) < 0.01f) {
                body.jump(JUMPING_SPEED);
                //animation for jumping
                body.animation(code, true, false);
            }
        } else if (code == KeyEvent.VK_LEFT) {
            right = false;
            body.startWalking(-WALKING_SPEED);
            //if its in the air and moving left play left jump amination
            if(Math.abs(body.getLinearVelocity().y) > 0.01f){
                body.animation(code, true, false);
            }else{
                //animtation for walking left
                body.animation(code, false, false);
            }
            
        } else if (code == KeyEvent.VK_RIGHT) {
            right = true;
            body.startWalking(WALKING_SPEED);
            //if its in the air and moving left play right jump amination
            if(Math.abs(body.getLinearVelocity().y) > 0.01f){
                body.animation(code, true, false);
            }else{
                //animation for walking right
                body.animation(code, false, false);
            }
        }
    }
    /**
     * Allows the player to stop walking once key has been released and applies
     * the idle animation.
     * <p>It also allows the player to shoot a bullet when X has been released</p>
     * @param e description of the key event
     */
    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_LEFT) {
            right = false;
            body.stopWalking();
            //stop walk animation and play idle stance
            body.animation(code, false, true);
        } else if(code == KeyEvent.VK_RIGHT){
            right = true;
            body.stopWalking();
            //stop walk animation and play idle stance
            body.animation(code, false, true);
        }else if( code == KeyEvent.VK_X){
            world.shootBullet(right);
        }
    }
}
