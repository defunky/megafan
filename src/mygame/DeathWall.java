/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.BodyImage;
import city.cs.engine.BoxShape;
import city.cs.engine.CollisionEvent;
import city.cs.engine.DynamicBody;
import city.cs.engine.Sensor;
import city.cs.engine.SensorEvent;
import city.cs.engine.SensorListener;
import city.cs.engine.Shape;
import city.cs.engine.World;
import org.jbox2d.common.Vec2;

/**
 * A spiked wall used in level 4 that the player must outrun, implements SensorListener
 * @author Abdalrahmane
 */
public class DeathWall extends DynamicBody implements SensorListener{
    private Player player;
    private GameLevel level;
    
    /**
     * Implements a SensorListener to allow movement whilst responding to
     * any contacts and allow movement through bodies
     * @param player the current player
     * @param level the current level
     */
    public DeathWall(Player player, GameLevel level) {
        super(level);
        this.player = player;
        this.level = level;
        Shape wallShape = new BoxShape(1.5f, 15);
        Sensor s = new Sensor(this, wallShape);
        setPosition(new Vec2(-20, 0));
        setLinearVelocity(new Vec2(5.5f,0));
        setGravityScale(0);
        s.addSensorListener(this);
        addImage(new BodyImage("data/misc/spikes.gif",60));
    }

    /**
     * Checks whether it has contacts with the player, if so damage the player
     * and reset the position of player and wall back to the start of the stage.
     * However if the player has no lives remaining game is over
     * @param e description of the sensor event, in this case it is the spiked wall
     */
    @Override
    public void beginContact(SensorEvent e) {
        if(e.getContactBody() == player){
            player.hurt();
            setPosition(new Vec2(-20, 0));
            player.setPosition(level.startPosition());
        }
    }
    /**
     * This method is not used.
     * @param e SensorEvent
     */
    @Override
    public void endContact(SensorEvent e) {
       
    }
}
