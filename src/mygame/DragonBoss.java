/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import org.jbox2d.common.Vec2;

/**
 * A boss used in level5, shoots the player with projectile every 2 seconds.
 * This class inherits TimerEnemy
 * @author Abdalrahmane
 */
public class DragonBoss extends TimerEnemy implements ActionListener {
    private static final int RANGE_DISTANCE = 30;
    private static final int WALKING_SPEED = 1;
    private static final int JUMPING_SPEED = 0;
    private int health = 30;
    private SolidFixture fixture;
    private static Shape enemyShape;
    private BodyImage bodyImage;
    
    private Timer timer;
    private ArrayList<DragonFire> fireList = new ArrayList<DragonFire>();
    private int fireRemover;
    private int animateCount;
    private Player player;
    private GameLevel world;
    
    /**
     * Creates the Dragon and sets health/range/walking speed/jump speed
     * Initializes the timer that shoots DragonFile every 2 seconds.
     * Dragon is unaffected by gravity.
     * <p>Walking Speed = 1 </p>
     * <p>Jumping Speed = 0 </p>
     * <p>Health = 30</p>
     * <p>Range Distance = 30</p>
     * @param world the current level
     * @param player the current player
     */
    public DragonBoss(GameLevel world, Player player) {
        super((World)world, player);
        setHealth(health);
        setRangeDistance(RANGE_DISTANCE);
        setWalkingSpeed(WALKING_SPEED);
        setJumpSpeed(JUMPING_SPEED);
        enemyShape = new BoxShape(5,7.5f);
        fixture = new SolidFixture(this, enemyShape);
        fixture.setDensity(1000);
        setGravityScale(0);
        bodyImage = new BodyImage("data/dragon/idle.gif",15);
        addImage(bodyImage);
        this.world = world;
        this.player = player;
        timer = new Timer(2000, this);
        timer.start();
        animateCount = 1;
        fireRemover = 0;
    }
    
    @Override
    public void hurt(){
        hitSound();
        if(health <= 1){
            player.increaseScore();
            timer.stop();
            destroy();
        }else health--; 
    }

    @Override
    public SolidFixture getFixture() {
        return fixture;
    }

    @Override
    public void startPosition(Vec2 startPos) {
        setPosition(startPos);
    }
    
    /**
     * Pauses the timer preventing from any projectiles been fired
     */
    @Override
    public void pauseTimer(){
        timer.stop();
    }
    
    /**
     * resumes the timer
     */
    @Override
    public void startTimer(){
        timer.start();
    }
        
    /**
     * Creates an instance of DragonFire every 2 seconds. Each instance
     * is placed in an array list, once there has been more than 4 instances of
     * the projectile it will begin to remove the oldest instance and destroy it
     * this to prevent large amount of projectiles that do not hit the player stay in simulation
     * causing slow down in the game. There is also animation counter that will change the apperance giving it an animation
     * @param e description of the ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        float playerX = player.getPosition().x;
        float enemyX = this.getPosition().x;
        float distance = playerX - enemyX;
        
        if(enemyX < playerX && distance < RANGE_DISTANCE){
            fireList.add(new DragonFire(world,this));
            animate("attack");
            if(fireList.size() > 4){
               fireList.get(fireRemover).destroy();
               fireRemover++;
            }

            for(DragonFire f : fireList){
                f.addCollisionListener(new EnemyBulletListener(player));
            }
            if(animateCount % 2 == 0) animate("idle");
            animateCount++;
        }
    }

    /**
     * Animates the dragon depending on the current state it is in.
     * Removes the current body image and sets a new one.
     * @param currentState the current state of the enemy.
     */
    @Override
    public void animate(String currentState) {
        removeImage(bodyImage);
        switch (currentState) {
            case "idle":
                bodyImage = new BodyImage("data/dragon/idle.gif",15);
                break;
            case "attack":
                bodyImage = new BodyImage("data/dragon/attack.gif",15);
                break;
        }
        addImage(bodyImage);
    }


    
}
