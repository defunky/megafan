/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The projectile shot by DragonBoss to damage the player
 * @author Abdalrahmane
 */
public class DragonFire extends DynamicBody{
    private static BodyImage bodyImage;
    private DragonBoss dragon;
    private static SoundClip gameMusic;

    /**
     * The projectile calculates the player and uses vectors to aim it at the time
     * it is fired. Using the vector coord of the player and dragon it subtracts it
     * then normalises it to get one unit which is then multiplied by 5 and impulse is applied to it. 
     * <p>There is a slight offset at which the project comes from to give the appearance that dragon shoots it from the mouth,
     * which the vector must take into account otherwise it can cause inaccurate projectiles.</p>
     * <p> The class also sets setBullet method (inherited from dynamic bodies) to true 
     * as result setting the bullet attribute to true turns on continuous collision detection between this body 
     * and other dynamic bodies, which is considerably more expensive. It is usually only necessary for small, 
     * fast-moving objects (like bullets).</p>
     * @param world current level
     * @param dragon current dragon
     */
    public DragonFire(GameLevel world, DragonBoss dragon) {
        super(world);
        this.dragon = dragon;
        setBullet(true);
        Shape bulletShape = new CircleShape(0.65f);
        SolidFixture fixture = new SolidFixture(this, bulletShape);
        bodyImage = new BodyImage("data/dragon/fire.gif");
        addImage(bodyImage);
        float x = dragon.getPosition().x+6;
        float y = dragon.getPosition().y+2;
        setPosition(new Vec2(x,y));
        
        Vec2 d = dragon.getPosition();
        //this is done as the dragons position has an offset set above as it shoots from the mouth
        Vec2 p = new Vec2(world.getPlayer().getPosition().x-6,world.getPlayer().getPosition().y-2);
        Vec2 v = p.sub(d);
        v.normalize();
        applyImpulse(v.mul(30.0f));
        
        setGravityScale(0);
        
        try {
            gameMusic = new SoundClip("data/music/shoot.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
}