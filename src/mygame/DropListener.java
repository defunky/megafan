/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * A CollisionListener used for static long body at the bottom of each stage,
 * allowing the lose a life and have it's position set back to the start of the level.
 * @author Abdalrahmane
 */
public class DropListener implements CollisionListener {
    private Player player;
    private GameLevel level;
    DropListener(Player player, GameLevel level){
        this.player = player;
        this.level = level;
    }
    
    /**
     * When a player falls off stage it will damage and set the player back at the start at level.
     * 
     * @param e description of the collision event, in this case the the drop platform
     */
    @Override
    public void collide(CollisionEvent e) {
        if(e.getOtherBody() == player){
            player.hurt();
            player.setPosition(level.startPosition());
        }
    }
}
