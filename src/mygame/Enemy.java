/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.BodyImage;
import city.cs.engine.SolidFixture;
import city.cs.engine.SoundClip;
import city.cs.engine.Walker;
import city.cs.engine.World;
import java.io.IOException;
import org.jbox2d.common.Vec2;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * An abstract enemy class which all enemies in the game inherit, TimerEnemy is another 
 * abstract class which also inherits this class also.
 * @author Abdalrahmane
 */
public abstract class Enemy extends Walker {
    private static SoundClip sc;
    private Player player;
    private int health, walkingSpeed, rangeDistance, jumpSpeed;

    /**
     * The constructor of the Enemy class, allowing the enemy to be created
     * and initalizes the player to allow scoring
     * @param world current level
     * @param player current player
     */
    public Enemy(World world, Player player) {
        super(world);
        this.player = player;
    }
    
    /**
     * An abstract method which returns the hittable fixture on enemy. 
     * E.G boxEnemy can only be damaged through the faceFixture
     * @return SolidFixture
     */
    public abstract SolidFixture getFixture();
    /**
     * Sets the spawning position of the enemy.
     * @param startPos Vec2 of the spawning coords
     */
    public abstract void startPosition(Vec2 startPos);
    /**
     * Allows animation within the enemies depending on the state they're in. E.G walking left, right or idle.
     * @param currentState current state of the enemy
     */
    public abstract void animate(String currentState);
    
    /**
     * A method to return itself, it's used in conjuction with EnemyStepListener to create AI.
     * @return Enemy
     */
    public Enemy getSelf(){
        return this;
    }
    /**
     * A simple set method to allow different health amounts for different enemies.
     * @param health amount of health the enemies has
     */
    public void setHealth(int health){
        this.health = health;
    }
    /**
     * A get method for health
     * @return health
     */
    public int getHealth(){
        return health;
    }
    
    /**
     * This method is called when the bullet collides with the hittable fixture,
     * it will cause the enemy to lose health and finally die (destroy) if hit enough times, resulting in a increase in player's score.
     * The method will also play a soundclip to signify that the enemy has been hit.
     */
    public void hurt(){
        health--;
        if(health < 1){
            player.increaseScore();
            destroy();
        }
        try {
            sc = new SoundClip("data/music/hit.wav");   // Open an audio input stream
            sc.setVolume(0.5f);
            sc.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
    /**
     * Returns walking speed of the enemy
     * @return walkingSpeed
     */
    public int getWalkingSpeed() {
        return walkingSpeed;
    }
    /**
     * Sets the walking speed of the enemy
     * @param walkingSpeed how fast the enemy should move
     */
    public void setWalkingSpeed(int walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }
    /**
     * returns the range distance
     * @return rangeDistance
     */
    public int getRangeDistance() {
        return rangeDistance;
    }
    /**
     * Sets the range distance of the enemy. Range distance is the distance at which the enemy
     * will notice the player and start to follow and attack him.
     * @param rangeDistance the range at which the enemy begins to move towards the player
     */
    public void setRangeDistance(int rangeDistance) {
        this.rangeDistance = rangeDistance;
    }
    /**
     * returns the jumping speed of the enemy
     * @return jumpSpeed
     */
    public int getJumpSpeed() {
        return jumpSpeed;
    }
    /**
     * Sets the jumping speed of the enemy.
     * @param jumpSpeed the speed at which the enemy jumps at
     */
    public void setJumpSpeed(int jumpSpeed) {
        this.jumpSpeed = jumpSpeed;
    }

}
