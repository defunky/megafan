/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Similar to DragonFire, it is projectile shot by HelmEnemy to damage the player.
 * This can be used a base for enemy projectiles as DragonFire is slightly different.
 * @author Abdalrahmane
 */
public class EnemyBullet extends DynamicBody{
    private static BodyImage bodyImage;
    private TimerEnemy enemy;
    private static SoundClip gameMusic;
    private float x,y;
    private Vec2 p;

    /**
     * The projectile calculates the player and uses vectors to aim it at the time
     * it is fired. Using the vector coord of the player and enemy it subtracts it
     * then normalises it to get one unit which is then multiplied by 5 and impulse is applied to it.
     * Gravity does not effect the bullet either.
     * <p> The class also sets setBullet method (inherited from dynamic bodies) to true 
     * as result setting the bullet attribute to true turns on continuous collision detection between this body 
     * and other dynamic bodies, which is considerably more expensive. It is usually only necessary for small, 
     * fast-moving objects (like bullets).</p>
     * @param world current level
     * @param enemy current enemy
     * @param right whether enemy is facing right or left
     */
    public EnemyBullet(GameLevel world, TimerEnemy enemy, boolean right) {
        super(world);
        this.enemy = enemy;
        setBullet(true);
        Shape bulletShape = new CircleShape(0.25f);
        SolidFixture fixture = new SolidFixture(this, bulletShape);
        bodyImage = new BodyImage("data/helmenemy/bullet.gif",0.5f);
        addImage(bodyImage);
        if(!right){
            x = enemy.getPosition().x-1.25f;
        }else{
            x = enemy.getPosition().x+1.25f;
        }
        y = enemy.getPosition().y;
        setPosition(new Vec2(x,y));
        
        Vec2 d = enemy.getPosition();
        
        if(!right){
            p = new Vec2(world.getPlayer().getPosition().x+1.25f,world.getPlayer().getPosition().y);
        }else{
            p = new Vec2(world.getPlayer().getPosition().x-1.25f,world.getPlayer().getPosition().y);
        }
        
        Vec2 v = p.sub(d);
        v.normalize();
        applyImpulse(v.mul(2.0f));
        
        setGravityScale(0);
        
        try {
            gameMusic = new SoundClip("data/music/shoot.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
}