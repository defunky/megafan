/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.CollisionEvent;
import city.cs.engine.CollisionListener;
import org.jbox2d.common.Vec2;

/**
 * CollisionListener for Enemy projectiles, Allows the player to get hurt and die
 * from the projectile shot by an enemy.
 * @author Abdalrahmane
 */
public class EnemyBulletListener implements CollisionListener {
    private Player player;
    public EnemyBulletListener(Player player){
        this.player = player;
    }

    @Override
    public void collide(CollisionEvent e) {
        if(e.getOtherBody() == player){
            player.hurt();
            e.getReportingBody().destroy();
        }else{
            e.getReportingBody().destroy();
        }
    }
    
}
