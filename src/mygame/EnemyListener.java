/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.CollisionEvent;
import city.cs.engine.CollisionListener;
import org.jbox2d.common.Vec2;

/**
 * A collision listener used to see if the player has collided with the enemy.
 * @author Abdalrahmane
 */
public class EnemyListener implements CollisionListener {
    private Player player;
    private Enemy enemy;
    
    public EnemyListener(Player player, Enemy enemy){
        this.player = player;
        this.enemy = enemy;
    }
    
    /**
     * Checks if the player has collided with enemy, if so hurt method is called in Player class
     * and a force will be applied on the opposite direction the player is facing.
     * @param e CollisionEvent
     */
    @Override
    public void collide(CollisionEvent e) {
        if(e.getOtherBody() == player){
            player.hurt();
            if(player.getPosition().x >= enemy.getPosition().x){
                player.setLinearVelocity(new Vec2(10,5));
                player.applyForce(new Vec2(150,5));
            }else{
                player.setLinearVelocity(new Vec2(-10,5));
                player.applyForce(new Vec2(-150,5));
            }
        }
    }
}
