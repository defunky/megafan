/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * A simple AI that calculates the current enemy position and the current players position
 * this is used to follow the player and try to collide with it, cause damage to the player.
 * 
 * @author Abdalrahmane
 */
public class EnemyStepListener implements StepListener{
    private Player player;
    private Enemy enemy;
    private static int walkingSpeed;
    private static int rangeDistance;
    private static int jumpSpeed;

    /**
     * Passes in the Player and enemy, then retrieves the walking speed, range distance and jumping speed
     * of the enemy, allowing different type of behaviours.
     * @param player current player
     * @param enemy current enemy
     */
    EnemyStepListener(Player player, Enemy enemy){
        this.player = player;
        this.enemy = enemy;
        walkingSpeed = enemy.getWalkingSpeed();
        rangeDistance = enemy.getRangeDistance();
        jumpSpeed = enemy.getJumpSpeed();
    }
   
    /**
     * not used
     * @param e StepEvent
     */
    @Override
    public void preStep(StepEvent e) {
    }
    
    /** Calculates distances between enemy and player, 
     * if player is within a certain distance, the enemy will begin chasing the player
     * will follow the player on either side depending on which side the player is in.
     * It also changes the enemies animation depending on its current behaviour
     * @param e StepEvent
     */
    @Override
    public void postStep(StepEvent e){

        float playerX = player.getPosition().x;
        float enemyX = enemy.getPosition().x;
        float distance = playerX - enemyX;
        
        if(enemyX < playerX && distance < rangeDistance){
            enemy.startWalking(walkingSpeed);
            enemy.jump(jumpSpeed);
            enemy.animate("right");
        }else if(enemyX > playerX && distance > -rangeDistance){
            enemy.startWalking(-walkingSpeed);
            enemy.jump(jumpSpeed);
            enemy.animate("left");
        }else{
            enemy.stopWalking();
            enemy.animate("idle");
        }
    } 
       
}
