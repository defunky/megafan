/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.ArrayList;
import org.jbox2d.common.Vec2;

/**
 * An abstract classes used to populate different levels within the game.
 * All levels extends this class.
 * @author Abdalrahmane
 */

public abstract class GameLevel extends World{
    private Player player;
    private Portal portal;
    private ArrayList<Bullet> bulletList = new ArrayList<Bullet>();
    private int bulletRemover = 0;
    private int target = 0;
    /**
     * The constructor is used to create a higher FPS cap (60 FPS), as the default 24
     * feels too jittery.
     */
    public GameLevel(){
        super(60);
    }
    
    /**
     * This method is called when a new level has started, it creates a new player
     * and portal and sets their position to different location depending on the level
     * 
     * @param game the current instance of the game
     */
    public void populate(MyGame game){
        player = new Player(this);
        player.setPosition(startPosition());
        portal = new Portal(this);
        portal.setPosition(portalPosition());
        portal.addCollisionListener(new PortalListener(game)); 
    }
    
    /**
     * An abstract method that is used to set the players starting position
     * @return Vec2
     */
    public abstract Vec2 startPosition();
    /**
     * An abstract method that is used to set the portals position
     * @return Vec2
     */
    public abstract Vec2 portalPosition();
    //public abstract void shootBullet(boolean characterSide);
    /**
     * An abstract method used to check whether the player has fulfilled level
     * target score, if return is true then the player can proceed to the next level
     * through the portal
     * @return boolean
     */
    public abstract boolean isCompleted();
    
    /**
     * A get method used to return the current player object
     * @return player
     */
    public Player getPlayer(){
        return player;
    }
    
    /**
     * This method is called when the player shoots a bullet using 'X'.
     * It checks which side the player is currently facing and creates a Bullet object,
     * this is then added to an ArrayList to keep track of the bullets. If there are more than 4 bullets on screen
     * then it will remove the latest one. Each bullet will also have a collision listener added to it.
     * @param characterSide if the character last movement was right or left
     */
    public void shootBullet(boolean characterSide){        
        //Bullet bullet = new Bullet(this, player);
        bulletList.add(new Bullet(this,getPlayer(),characterSide));
        if(bulletList.size() > 4){
            bulletList.get(bulletRemover).destroy();
            //bulletList.remove(1);
            //System.out.println(bulletList.size());
            bulletRemover++;
        }
        for(Bullet bullet : bulletList){
            bullet.addCollisionListener(new BulletListener(getPlayer()));
        }
    }
    /**
     * Sets the target score needed to complete a level
     * @param target amount of points needed to complete the level
     */
    public void setTarget(int target){
        this.target = target;
    }
    /**
     * returns the targets score
     * @return target
     */
    public int getTarget(){
        return target;
    }

    
    
}
