/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * A Enemy Boss used in Level 3, it has sigificantly more health than typical enemy and can also jump around
 * @author Abdalrahmane
 */
public class HelmBoss extends Enemy {
    private static final int WALKING_SPEED = 2;
    private static final int RANGE_DISTANCE = 15;
    private static final int JUMPING_SPEED = 6;
    private static final int HEALTH = 50;
    private SolidFixture fixture;
    private static final Shape enemyShape = new PolygonShape(
            -3.81f,0.22f, -2.24f,3.59f, -0.02f,3.99f, 2.25f,3.59f, 3.8f,0.19f, 3.68f,-3.95f, -3.71f,-3.98f, -3.82f,0.09f);
    private BodyImage bodyImage;
    /**
     * Creates the enemy boss in the current level. The boss jumps around and tries to collide with the player.
     * The enemy has lower gravity scale to allow the player to run underneath it while it jumps. 
     * <p>Walking Speed = 2 </p>
     * <p>Jumping Speed = 6</p>
     * <p>Health = 50</p>
     * <p>Range Distance = 15</p>
     * @param world the current world
     * @param player the current player
     */
    public HelmBoss(World world, Player player) {
        super(world,player);
        setHealth(HEALTH);
        setWalkingSpeed(WALKING_SPEED);
        setJumpSpeed(JUMPING_SPEED);
        setRangeDistance(RANGE_DISTANCE);
        fixture = new SolidFixture(this, enemyShape);
        fixture.setDensity(30);
        setGravityScale(0.4f);
        bodyImage = new BodyImage("data/lvl3boss/idle.gif",8);
        addImage(bodyImage);
    }
    
    @Override
    public SolidFixture getFixture() {
        return fixture;
    }

    @Override
    public void startPosition(Vec2 startPos) {
        setPosition(startPos);
    }

    @Override
    public void animate(String currentState) {
        //has no animations
    }
    
}
