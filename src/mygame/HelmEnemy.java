/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.BodyImage;
import city.cs.engine.BoxShape;
import city.cs.engine.Shape;
import city.cs.engine.SolidFixture;
import city.cs.engine.World;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import org.jbox2d.common.Vec2;

/**
 * An Enemy that will shoot projectiles and walk towards the player in level 1
 * @author Abdalrahmane
 */
public class HelmEnemy extends TimerEnemy implements ActionListener{
    private static final int WALKING_SPEED = 2;
    private static final int RANGE_DISTANCE = 10;
    private static final int JUMPING_SPEED = 0;
    private int health = 3;
    private Timer timer;
    private SolidFixture fixture;
    private static Shape enemyShape;
    private BodyImage bodyImage;
    private Player player;
    private GameLevel world;
    private ArrayList<EnemyBullet> enemyBList = new ArrayList<EnemyBullet>();
    private int enemyBulletRemover;
    /** 
     * The Constructor of helmEnemy, creates the fixtures and sets the amount of health
     * walking speed, jump speed and range distance
     * <p>Walking Speed = 2</p>
     * <p>Jumping Speed = 0</p>
     * <p>Health = 3</p>
     * <p>Range Distance = 10 </p>
     * @param world the current world
     * @param player the current player
     */
    public HelmEnemy(GameLevel world, Player player) {
        super((World)world, player);
        setHealth(health);
        setWalkingSpeed(WALKING_SPEED);
        setRangeDistance(RANGE_DISTANCE);
        setJumpSpeed(JUMPING_SPEED);
        enemyShape = new BoxShape(0.75f,0.75f);
        fixture = new SolidFixture(this, enemyShape);
        fixture.setDensity(1000);
        bodyImage = new BodyImage("data/helmenemy/idle.gif",1.5f);
        addImage(bodyImage);
        this.world = world;
        this.player = player;
        timer = new Timer(1500, this);
        timer.start();
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void hurt(){
        hitSound();
        if(health <= 1){
            player.increaseScore();
            timer.stop();
            destroy();
        }else health--; 
    }

    @Override
    public SolidFixture getFixture() {
        return fixture;
    }


    @Override
    public void startPosition(Vec2 startPos) {
        setPosition(startPos);
    }

    /**
     * This method is called every 1500 milliseconds, it calculates players x position
     * and it's own x position. The method then calculates whether the player is within the enemy
     * range distance and which side the player is at. If the player is within
     * the distance it will create EnemyBullet object that can hurt and kill the player.
     * <p>When a EnemyBullet object is created it's placed into an arraylist, 
     * and begins to remove the bullets if it exceeds 4 bullets at a time; with the latest one being removed</p>
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        float playerX = player.getPosition().x;
        float enemyX = this.getPosition().x;
        float distance = playerX - enemyX;
        
        if(enemyX > playerX && distance > -RANGE_DISTANCE){
            enemyBList.add(new EnemyBullet(world,this,false));
        }else if(enemyX < playerX && distance < RANGE_DISTANCE){
            enemyBList.add(new EnemyBullet(world,this,true));
        }
        
        if(enemyBList.size() > 4){
           enemyBList.get(enemyBulletRemover).destroy();
           enemyBulletRemover++;
        }
        for(EnemyBullet b : enemyBList){
            b.addCollisionListener(new EnemyBulletListener(player));
        }
    }

    @Override
    public void pauseTimer() {
        timer.stop();
    }

    @Override
    public void startTimer() {
        timer.start();
    }

    @Override
    public void animate(String currentState) {
        removeImage(bodyImage);
        switch (currentState) {
            case "right":
                bodyImage = new BodyImage("data/helmenemy/walkright.gif",1.5f);
                break;
            case "left":
                bodyImage = new BodyImage("data/helmenemy/walk.gif",1.5f);
                break;
            case "idle":
                bodyImage = new BodyImage("data/helmenemy/idle.gif",1.5f);
                break;
        }
        addImage(bodyImage);
    }
    
}
