/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.awt.BorderLayout;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.*;
import javax.swing.table.TableModel;

/**
 * This class reads/writes highscore from a txt file which
 * is displayed in JTable that accessible through JMenuBar.
 * @author Abdalrahmane
 */
public class HighScore extends JFrame {
    private FileReader fr;
    private BufferedReader bf;
    private String fileName;
    private List<HighScorePlayer> list = new ArrayList<HighScorePlayer>();
    
    public HighScore(){
        fileName = "data/highscore.txt";
    }
    
    /** 
     * An arraylist is created that uses HighScorePlayer object, using a bufferreader
     * it will begin read the text document. Each line of the text document will have player name
     * and score that is separated with a comma; both the player name and score will be passed to
     * HighScorePlayer object. The list is then sorted in reverse order placing top scoring at the top 
     * of the list.
     */
    
    public void readHighScore(){
        list.clear();
        try{
            fr = new FileReader(fileName);
            bf = new BufferedReader(fr);
            String line = null;
            while ((line = bf.readLine()) != null) {
                String[] tokens = line.split(",");
                String name = tokens[0];
                int score = Integer.parseInt(tokens[1]);
                list.add(new HighScorePlayer(name, score));
                Collections.sort(list, Collections.reverseOrder());
            }
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            try {
                fr.close();
                bf.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    
    }
    /** Creates a JTable in a new window and fills it up with highscore data from the arraylist, 
     * the JTable uses HighScoreTableModel instead of the DefaultTableModel doesn't allow the use of custom classes
     * in this case HighScorePlayer.
    */
    public void viewHighScore(){
        String columnNames[] = { "Name", "Score" };
        readHighScore();
        setTitle( "Highscore Table" );
        setSize( 600, 300 );
        // Create a panel to hold all other components
        JPanel  topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        
        // Create a new table instance
        TableModel model = new HighScoreTableModel(list);
        JTable table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        topPanel.add(scrollPane, BorderLayout.CENTER );
        setVisible(true);
    }
    
    /** Passes the current players score if the player has achieved a highscore.
     * Method asks for the players name through dialogbox which is then added to the array list,
     * its then sorted and the player with least score is removed to keep the list with 10 players only
     * it overwrites original txt file to keep the names in score order  
     * opposed to having the most recent player at the bottom.
     * @param score the current players high score
    */
    public void writeHighScore(int score){
        String name = JOptionPane.showInputDialog("Congratulations, You are on the leaderboard"
                + "\nPlease enter your name!");
        list.add(new HighScorePlayer(name,score));
        Collections.sort(list, Collections.reverseOrder());
        if(list.size() > 10) list.remove(10);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName));
            for(HighScorePlayer player : list){
                writer.write(player.getName() + "," + player.getScore());
                writer.newLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            readHighScore();
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    /**
     * This method is called when the player looses all his lives.
     * The method will go through current high score holders and see if player has achieved higher score
     * than any of the current holders, if so it will call the writeHighScore method.
     * 
     * @param playerScore the current players score
     */
    public void checkScore(int playerScore){
        readHighScore();
        boolean check = false;
        for(HighScorePlayer player : list){
            if(playerScore > player.getScore()){ 
                check = true;
            }
        }
        if(check) writeHighScore(playerScore);
    }
}
