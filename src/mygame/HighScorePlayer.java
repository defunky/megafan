/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 * This class is used to keep track of the Player names and their high scores.
 * The class implements Comparable to allow sorting to display scores in correct order
 * @author Abdalrahmane
 */
public class HighScorePlayer implements Comparable<HighScorePlayer> {
    private int score;
    private String name;
    
    /**
     * Constructor - creates HighScorePlayer object with specified name and score
     * @param name The selected name that player has input
     * @param score The score that is linked with that player
     */
    public HighScorePlayer(String name, int score){
        this.name = name;
        this.score = score;
    }
    /**
     * Returns the name of the player
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * Returns the score of the player
     * @return score
     */
    public int getScore(){
        return score;
    }
    /**
     * Returns a string representation of the object. Used for debugging
     * @return name and score
     */
    @Override
    public String toString(){
        return name +" : " +score;
    }

    /**
     * Compares two HighScorePlayer objects, this is used in HighScore class.
     * It's called through Collection.sort() allowing sorting of players through their score,
     * creating a ranked leader board. 
     * @param p HighScorePlayer
     * @return 0 if values are equal - 1 if this score is greater than passed object score -
     *  -1 if this score is less than passed object score
     */
    @Override
    public int compareTo(HighScorePlayer p) {
        int score2 = p.getScore();
        if (score == score2){
            return 0;
        }else if (score > score2){
            return 1;
        }else{
            return -1;
        }
    }
    
}
