/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * A Custom table model used to display high scores as the DefaultTableModel does 
 * not allow ArrayLists with custom classes.
 * 
 */
public class HighScoreTableModel extends AbstractTableModel {
    private List<HighScorePlayer> list;
    private String columnNames[];
    
    public HighScoreTableModel(List<HighScorePlayer> list){
        super();
        this.list = list;
        columnNames = new String[]{ "Name", "Score" };
    }

    /**
     * Returns row count
     * @return size of the list
     */
    @Override
    public int getRowCount() {
        return list.size();
    }
    
    /**
     * returns column count
     * @return the amount of columns
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * The object to render at a cell in this case the player name and score
     * @param row -  Each individuals player in the table (Row)
     * @param column - Each Column, Player name and score
     * @return players name or players score
     */
    @Override
    public Object getValueAt(int row, int column) {
        HighScorePlayer player = list.get(row);
        switch(column){
            case 0: return player.getName();
            case 1: return player.getScore();
            default: return null;
        }
    }
    
    /**
     * Get column names
     * @param col column
     * @return column name of a given column 
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col] ;
    }
    
}
