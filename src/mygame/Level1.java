/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.*;
import org.jbox2d.common.Vec2;
    /**
     * The first level of the game, contains two different enemies (BoxEnemy and HelmEnemy). 
     * The target score for this level is 5.
     */
public class Level1 extends GameLevel {
    private BoxEnemy boxEnemy;
    private HelmEnemy helmEnemy;
    private final int SCORE_NEEDED = 0;
    /**
     * Populates the level, and creates the platforms and enemies. Sets the target score
     * @param game current instance of the game
     */
    @Override
    public void populate(MyGame game) {
        super.populate(game);
        setTarget(SCORE_NEEDED);
        //make the platform for the bodies to stand on
        {
            Shape groundShape = new BoxShape(49, 2);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(0, -14.5f));
            ground.addImage(new BodyImage("data/misc/floor.gif",5.5f),new Vec2(0,-0.75f));
            
            Shape dropShape = new BoxShape(150, 0);
            Body drop = new StaticBody(this, dropShape);
            drop.setPosition(new Vec2(60,-25f));
            drop.addCollisionListener(new DropListener(getPlayer(), this));
        }
        {
            Shape groundShape = new BoxShape(8, 2);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(49.5f, -9));
            ground.addImage(new BodyImage("data/misc/platform.gif",5.5f),new Vec2(0,-0.75f));
            NoFrictionPanel p = new NoFrictionPanel(this, 8, 2, 49.5f, -9);

            
            Body platform1 = new StaticBody(this, groundShape);
            platform1.setPosition(new Vec2(57.65f, -3.4f));
            platform1.addImage(new BodyImage("data/misc/platform.gif",5.5f),new Vec2(0,-0.75f));
            NoFrictionPanel p1 = new NoFrictionPanel(this, 8, 2, 57.65f, -3.5f);

            
            Shape platform2Shape = new BoxShape(25,2);
            Body platform2 = new StaticBody(this, platform2Shape);
            platform2.setPosition(new Vec2(84.5f, 2.2f));
            platform2.addImage(new BodyImage("data/misc/platform2.gif",5.5f),new Vec2(0,-0.75f));
            NoFrictionPanel p2 = new NoFrictionPanel(this, 25, 2, 84.5f, 2.2f);
        }
        {
            Shape groundShape = new BoxShape(49, 2);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(125, -14.5f));
            ground.addImage(new BodyImage("data/misc/floor.gif",5.5f),new Vec2(0,-0.75f));
        }
            
        //life pickup
        LivePickup live = new LivePickup(this);
        live.startPosition(new Vec2(40,-7));
        live.addCollisionListener(new Pickup(getPlayer())); 
        
        //adds collision detection and simple AI for the enemy
       // boxEnemy.addCollisionListener(new BoxEnemyAI(getPlayer() ,boxEnemy,bullet));
           
//        boxEnemy = new BoxEnemy(this);
//        boxEnemy.startPosition(new Vec2(7,-8));
//        addStepListener(new BoxEnemyAI(getPlayer(), boxEnemy.getSelf()));
//        boxEnemy.addCollisionListener(new BoxEnemyAI(getPlayer(),boxEnemy.getSelf())); 
       
        List<Enemy> enemyList = new ArrayList<Enemy>();
        for(int i = 0; i < 4; i++){
            enemyList.add(boxEnemy = new BoxEnemy(this, getPlayer()));
            enemyList.get(i).startPosition(new Vec2(i*8,-8));
        }
        
        int j = 0;
        for(int i = 4; i < 8; i++){
            j = j + 5;
            enemyList.add(helmEnemy = new HelmEnemy(this, getPlayer()));
            enemyList.get(i).startPosition(new Vec2(65+(j*2),10));
        }
        for(Enemy enemy : enemyList){
            enemy.addCollisionListener(new EnemyListener(getPlayer(),enemy.getSelf()));
            addStepListener(new EnemyStepListener(getPlayer(), enemy.getSelf()));
        }
        
//       HelmEnemy helm = new HelmEnemy(this, getPlayer());
//       helm.setPosition(new Vec2(-20, 0));
//       addStepListener(new EnemyStepListener(getPlayer(), helm.getSelf()));

    }
    
    @Override
    public Vec2 startPosition() {
        return new Vec2(-10, -13);
    }
    
    @Override
    public Vec2 portalPosition(){
        return new Vec2(85,-10);
    }

    @Override
    public boolean isCompleted(){
        return getPlayer().getScore() >= SCORE_NEEDED;
    }
    
    
}
