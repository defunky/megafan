/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.*;
import org.jbox2d.common.Vec2;
    /**
     * The second level of the game, contains ShellEnemy. The target level
     * is 4 above the players current score. 
     */
public class Level2 extends GameLevel {
    private int SCORE_NEEDED;
    private ShellEnemy shellEnemy;
    /**
     * Populates the level, and creates the platforms and enemies. Sets the target score
     * @param game the current instance of the game
     */
    @Override
    public void populate(MyGame game) {
        super.populate(game);
        SCORE_NEEDED = getTarget() + 4;
        setTarget(SCORE_NEEDED);
        //make the platform for the bodies to stand on
        {
            Shape groundShape = new BoxShape(4.5f, 0.3f);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(-8, -1));
            ground.addImage(new BodyImage("data/misc/level2platform.gif"));
            NoFrictionPanel p = new NoFrictionPanel(this, 4.5f, 0.3f, -8, -1);
            
            Shape dropShape = new BoxShape(150, 0);
            Body drop = new StaticBody(this, dropShape);
            drop.setPosition(new Vec2(100,-25f));
            drop.addCollisionListener(new DropListener(getPlayer(), this));
            
            Body plat1 = new StaticBody(this, groundShape);
            plat1.setPosition(new Vec2(5, -1));
            plat1.addImage(new BodyImage("data/misc/level2platform.gif"));
            NoFrictionPanel p1 = new NoFrictionPanel(this, 4.5f, 0.3f, 5, -1);
            
            Shape faceplatShape = new BoxShape(5, 5);
            Body faceplat = new StaticBody(this, faceplatShape);
            faceplat.setPosition(new Vec2(20, -5));
            faceplat.addImage(new BodyImage("data/misc/level2faceplatform.gif",10));
            NoFrictionPanel p3 = new NoFrictionPanel(this, 5, 5, 20, -5);
            
            Shape plat2Shape = new BoxShape(9.5f,0.3f);
            Body plat2 = new StaticBody(this, plat2Shape);
            plat2.setPosition(new Vec2(40, -5));
            plat2.addImage(new BodyImage("data/misc/level2platform2.gif"));
            NoFrictionPanel p4 = new NoFrictionPanel(this, 9.5f, 0.3f, 40, -5);
            
            Body faceplat2 = new StaticBody(this, faceplatShape);
            faceplat2.setPosition(new Vec2(65, -5));
            faceplat2.addImage(new BodyImage("data/misc/level2faceplatform.gif",10));
            NoFrictionPanel p5 = new NoFrictionPanel(this, 5, 5, 65, -5);
            
            Body plat3 = new StaticBody(this, plat2Shape);
            plat3.setPosition(new Vec2(85, -5));
            plat3.addImage(new BodyImage("data/misc/level2platform2.gif"));
            NoFrictionPanel p6 = new NoFrictionPanel(this, 9.5f, 0.3f, 85, -5);

            Body plat4 = new StaticBody(this, groundShape);
            plat4.setPosition(new Vec2(105, -1));
            plat4.addImage(new BodyImage("data/misc/level2platform.gif"));
            NoFrictionPanel p7 = new NoFrictionPanel(this, 4.5f, 0.3f, 105, -1);

            
            Body plat5 = new StaticBody(this, groundShape);
            plat5.setPosition(new Vec2(120, -10));
            plat5.addImage(new BodyImage("data/misc/level2platform.gif"));
            NoFrictionPanel p8 = new NoFrictionPanel(this, 4.5f, 0.3f, 120, -10);
            
            Body plat6 = new StaticBody(this, groundShape);
            plat6.setPosition(new Vec2(135, -5));
            plat6.addImage(new BodyImage("data/misc/level2platform.gif"));
            NoFrictionPanel p9 = new NoFrictionPanel(this, 4.5f, 0.3f, 135, -5);
            
            Body plat7 = new StaticBody(this, plat2Shape);
            plat7.setPosition(new Vec2(155, 0));
            plat7.addImage(new BodyImage("data/misc/level2platform2.gif"));
            NoFrictionPanel p10 = new NoFrictionPanel(this, 9.5f, 0.3f, 155, 0);
            
            Body plat8 = new StaticBody(this, plat2Shape);
            plat8.setPosition(new Vec2(175, 5));
            plat8.addImage(new BodyImage("data/misc/level2platform2.gif"));
            NoFrictionPanel p11 = new NoFrictionPanel(this, 9.5f, 0.3f, 175, 5);

            
            Body plat9 = new StaticBody(this, plat2Shape);
            plat9.setPosition(new Vec2(200, 5));
            plat9.addImage(new BodyImage("data/misc/level2platform2.gif"));
            NoFrictionPanel p12 = new NoFrictionPanel(this, 9.5f, 0.3f, 200, 5);
        }
            
        //life pickup
        LivePickup live = new LivePickup(this);
        live.startPosition(new Vec2(135,-3));
        live.addCollisionListener(new Pickup(getPlayer())); 
        
        List<ShellEnemy> enemyList = new ArrayList<ShellEnemy>();
        for(int i = 0; i < 10; i++){
            enemyList.add(shellEnemy = new ShellEnemy(this,getPlayer()));
            enemyList.get(i).startPosition(new Vec2((i*15)+5,10));
        }
        
        for(ShellEnemy shellEnemy : enemyList){
            shellEnemy.addCollisionListener(new EnemyListener(getPlayer(),shellEnemy.getSelf()));
            addStepListener(new EnemyStepListener(getPlayer(), shellEnemy.getSelf()));
        }
    }
    
    @Override
    public Vec2 startPosition() {
        return new Vec2(-8, -1);
    }
    
    @Override
    public Vec2 portalPosition(){
        return new Vec2(210,8);
    }

    @Override
    public boolean isCompleted(){
        return getPlayer().getScore() >= SCORE_NEEDED;
    }
    
    
}
