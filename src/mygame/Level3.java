/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.*;
import org.jbox2d.common.Vec2;
    /**
     * The third level of the game, Boss stage. Contains HelmBoss Enemy.
     * Player must kill the boss to proceed.
     */
public class Level3 extends GameLevel {
    private int SCORE_NEEDED;
    private HelmBoss helmBoss;
    /**
     * Populates the level and creates the platform and Boss Enemy. Sets the target score
     * @param game the current instance of the game.
     */
    @Override
    public void populate(MyGame game) {
        super.populate(game);
        SCORE_NEEDED = getTarget() + 1;
        setTarget(SCORE_NEEDED);
        //make the platform for the bodies to stand on
        {
            Shape groundShape = new BoxShape(20, 2.5f);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(0, -14.5f));
            ground.addImage(new BodyImage("data/misc/lvl3platform.gif",5.5f));
            NoFrictionPanel p = new NoFrictionPanel(this, 20, 2.5f, 0, -14.5f);
            
            Shape dropShape = new BoxShape(40, 0);
            Body drop = new StaticBody(this, dropShape);
            drop.setPosition(new Vec2(0,-25f));
            drop.addCollisionListener(new DropListener(getPlayer(), this));
            
        }
        
        helmBoss = new HelmBoss(this,getPlayer());
        helmBoss.setPosition(new Vec2(8,-13));
        helmBoss.addCollisionListener(new EnemyListener(getPlayer(),helmBoss.getSelf()));
        addStepListener(new EnemyStepListener(getPlayer(), helmBoss.getSelf()));
            
        //life pickup
        LivePickup live = new LivePickup(this);
        live.startPosition(new Vec2(135,-3));
        live.addCollisionListener(new Pickup(getPlayer())); 
        
    }
    
    @Override
    public Vec2 startPosition() {
        return new Vec2(-8, -13);
    }
    
    
    @Override
    public Vec2 portalPosition(){
        return new Vec2(20,-9.5f);
    }

    @Override
    public boolean isCompleted(){
        return getPlayer().getScore() >= SCORE_NEEDED;
    }
    
    
}
