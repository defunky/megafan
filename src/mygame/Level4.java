/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.*;
import org.jbox2d.common.Vec2;
    /**
     * The fourth level in the game, the player must out run the spiked wall and
     * reach the portal on the other side of the stage.
     */
public class Level4 extends GameLevel{
    private final int SCORE_NEEDED = 0;
    private DynamicBody wall;
    int i = 0;
    /**
     * Populates the level and creates the platforms and Spiked Wall. Sets the target score.
     * @param game the current instance of the game
     */
    @Override
    public void populate(MyGame game) {
        super.populate(game);
        setTarget(SCORE_NEEDED);
        //make the platform for the bodies to stand on
        {
            Shape groundShape = new BoxShape(77.5f, 2.5f);
            Body ground = new StaticBody(this, groundShape);
            ground.setPosition(new Vec2(60, -15f));
            DeathWall d = new DeathWall(getPlayer(),this);
            ground.addImage(new BodyImage("data/misc/lvl4floor.gif",5.5f));
            //NoFrictionPanel p = new NoFrictionPanel(this, 20, 2.5f, 0, -14.5f);
            
            Shape wShape = new BoxShape(3, 10);
            Body w = new StaticBody(this, wShape);
            w.setPosition(new Vec2(65, -2.5f));
            NoFrictionPanel p = new NoFrictionPanel(this, 3, 10, 65, -2.5f);
            
            Shape wShape2 = new BoxShape(3, 5);
            Body w2 = new StaticBody(this, wShape2);
            w2.setPosition(new Vec2(85, -9));
            NoFrictionPanel p2 = new NoFrictionPanel(this, 3, 5, 85, -9);
                        
            Body w3 = new StaticBody(this, wShape);
            w3.setPosition(new Vec2(100, -9));
            NoFrictionPanel p3 = new NoFrictionPanel(this, 3, 10, 100, -9);
            
            Body w4 = new StaticBody(this, wShape);
            w4.setPosition(new Vec2(115, -2.5f));
            NoFrictionPanel p4 = new NoFrictionPanel(this, 3, 10, 115, -2.5f);
            
            
            Shape platShape = new BoxShape(4.5f, 0.3f);
            Body plat1 = new StaticBody(this, platShape);
            plat1.setPosition(new Vec2(55, 4));
            plat1.addImage(new BodyImage("data/misc/level2platform.gif"));
            //NoFrictionPanel p1 = new NoFrictionPanel(this, 4.5f, 0.3f, 5, -1);
            
            Body plat2 = new StaticBody(this, platShape);
            plat2.setPosition(new Vec2(40, -5));
            plat2.addImage(new BodyImage("data/misc/level2platform.gif"));
            //NoFrictionPanel p2 = new NoFrictionPanel(this, 4.5f, 0.3f, 5, -1);

            
            Shape dropShape = new BoxShape(90, 0);
            Body drop = new StaticBody(this, dropShape);
            drop.setPosition(new Vec2(60,-25f));
            drop.addCollisionListener(new DropListener(getPlayer(), this));
        }
    }
    
    @Override
    public Vec2 startPosition() {
        return new Vec2(-8, -10.8f);
    }
    
    
    @Override
    public Vec2 portalPosition(){
        return new Vec2(135,-9.5f);
    }

    @Override
    public boolean isCompleted(){
        return getPlayer().getScore() >= SCORE_NEEDED;
    }
 
}
