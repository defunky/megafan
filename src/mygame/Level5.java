/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.util.*;
import org.jbox2d.common.Vec2;
    /**
     * The final level in the game, contains DragonBoss. The player must kill the boss
     * to finish the game.
     */
public class Level5 extends GameLevel {
    private DragonBoss dragon;
    private int SCORE_NEEDED;
    /**
     * Populates the level, creates the platforms and Dragon Boss. Sets the target score.
     */
    @Override
    public void populate(MyGame game) {
        super.populate(game);
        SCORE_NEEDED = getTarget() + 1;
        setTarget(SCORE_NEEDED);
        //make the platform for the bodies to stand on
        {
            
            Shape dropShape = new BoxShape(150, 0);
            Body drop = new StaticBody(this, dropShape);
            drop.setPosition(new Vec2(60,-25f));
            drop.addCollisionListener(new DropListener(getPlayer(), this));
            
            Shape squareShape = new BoxShape(1,1);
            Body plat = new StaticBody(this, squareShape);
            plat.setPosition(new Vec2(5, 4));
            plat.addImage(new BodyImage("data/misc/tile.gif",2));
            NoFrictionPanel p = new NoFrictionPanel(this, 1, 1, 5, 4);
            
            Body plat2 = new StaticBody(this, squareShape);
            plat2.setPosition(new Vec2(10, 10));
            plat2.addImage(new BodyImage("data/misc/tile.gif",2));
            NoFrictionPanel p2 = new NoFrictionPanel(this, 1, 1, 10, 10);
            
            Body plat3 = new StaticBody(this, squareShape);
            plat3.setPosition(new Vec2(10, -3));
            plat3.addImage(new BodyImage("data/misc/tile.gif",2));
            NoFrictionPanel p3 = new NoFrictionPanel(this, 1, 1, 10, -3);

        }

       dragon = new DragonBoss(this, getPlayer());
       dragon.addCollisionListener(new EnemyListener(getPlayer(),dragon.getSelf()));
       dragon.setPosition(new Vec2(-10, 0));
//       HelmEnemy helm = new HelmEnemy(this, getPlayer());
//       helm.setPosition(new Vec2(0, 0));
//       addStepListener(new EnemyStepListener(getPlayer(), helm.getSelf()));
    }
    
    @Override
    public Vec2 startPosition() {
        return new Vec2(5, 8);
    }
    
    @Override
    public Vec2 portalPosition(){
        return new Vec2(15,15);
    }

    @Override
    public boolean isCompleted(){
        return getPlayer().getScore() >= SCORE_NEEDED;
    }
    
    
}
