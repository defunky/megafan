/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * A pickup that will increase the number of lives the player has
 * 
 */
public class LivePickup extends DynamicBody {
    private BodyImage bodyImage;

    /**
     * Creates an instance of live pickup on the current level. A collisionlistener
     * is applied to this to increase the players life.
     * @param world The current level
     */
    public LivePickup(World world) {
        super(world);
        Shape liveShape = new BoxShape(0.5f,0.5f);
        SolidFixture fixture = new SolidFixture(this, liveShape);
        bodyImage = new BodyImage("data/misc/livePickup.gif");
        addImage(bodyImage);
        //setPosition(new Vec2(0,-10));
    }
    /**
     * The spawn position of the live pickup
     * @param pos Vec2 position where live is added.
     */
    public void startPosition(Vec2 pos){
        setPosition(pos);
    }
}
