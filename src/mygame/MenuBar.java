/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

/**
 * A menu bar located on the top of the game window allowing the player to view the 
 * leaderboard and exit the game.
 * @author Abdalrahmane
 */
public class MenuBar extends JMenuBar implements ActionListener {
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem hsMenu,exitMenu;
    private HighScore highScore;
    
    /**
     * Creates the menu bar and menu bar items. Adds ActionListener to each of
     * menubar items to respond to user upon mouse press
     */
    public MenuBar(){
        highScore = new HighScore();
        // Creates a menubar for a JFrame
        menuBar = new JMenuBar();
        
        // Add the menubar to the frame
        add(menuBar);
        
        // Creates File menu to allow player to select
        fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        
        //Creates menu items within File Menu
        hsMenu = new JMenuItem("View High Scores");
        exitMenu = new JMenuItem("Quit");
        
        //add them to file menu
        fileMenu.add(hsMenu);
        fileMenu.add(exitMenu);
        
        //adds actionlisteners to them
        hsMenu.addActionListener(this);
        exitMenu.addActionListener(this);
    
    }

    /**
     * Called when user selects and menu item. Checks which menu item is pressed,
     * if its view high score then it will call viewHighScore method in HighScore else if
     * the user presses exit the game closes.
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == hsMenu){
            highScore.viewHighScore();
        }else{
            System.exit(0);
        }
    }


    
}
