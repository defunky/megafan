package mygame;

import city.cs.engine.*;
import java.awt.BorderLayout;
import javax.swing.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The main class used to set up and launch the game
 * @author Abdalrahmane
 */

public class MyGame {
    private Background view;
    private GameLevel world;
    private GameLevel[] level = new GameLevel[5];
    private int levelSelect = 0;
    private int lastScore,lastLives;
    private Controller controller;
    private SoundClip gameMusic;
    private ControlPanel cp;
    private MenuBar mb;

    /**
     * Intalizes the levels in level array, sets up the controls, view and JFrame to allow
     * the player to see the game
     */
    public MyGame(){
        level[0] = new Level1();
        level[1] = new Level2();
        level[2] = new Level3();
        level[3] = new Level4();
        level[4] = new Level5();
        world = level[0];
        world.populate(this);
        
        musicSettings();

        //creates view for the user to see
        view = new Background(world, 800, 650, getPlayer(),this);
        view.newBG();

        // display the view in a frame
        final JFrame frame = new JFrame("Megaman");

        // quit the application when the game window is closed
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //centers the window
        frame.setLocationByPlatform(true);
        // display the world in the window
        frame.add(view);


        //Creates and adds the control panel
        cp = new ControlPanel(world, this);
        mb = new MenuBar();
        frame.add(cp, BorderLayout.SOUTH);
        frame.add(mb, BorderLayout.NORTH);
        
        frame.setFocusable(true);
        //frame.requestFocusInWindow();
        // don't let the game window be resized
        frame.setResizable(false);
        // size the game window to fit the world view
        frame.pack();
        
        // make the window visible
        frame.setVisible(true);

        // uncomment this to make a debugging view
        JFrame debugView = new DebugViewer(world, 500, 500);
        //uncomment this to get grid view
         //view.setGridResolution(1);

        //passes player to allow it to be controlled and animates players direction
        controller = new Controller(world.getPlayer(),world);

        frame.addKeyListener(controller);
        //Follows the player rather the static camera
        world.addStepListener(new CameraFollow(view, world.getPlayer()));
        // starts level one!
        world.start();
    }
    
    /**
     * Returns the current instance of the player
     * @return player
     */
    public Player getPlayer(){
        return world.getPlayer();
    }
    /**
     * Checks to see if the player has achieved the target score, called when the player
     * collides with the portal.
     * @return boolean
     */
    public boolean isCompleted(){
        return world.isCompleted();
    }
   /**
    * This method controls which music level it will play as each level has a unique song.
    * The method loads up specific level song and loops it until the next level has been reached.
    */
    public void musicSettings(){
        try {
            if(levelSelect != 0) gameMusic.stop();
            gameMusic = new SoundClip("data/music/lvl"+(levelSelect+1)+".wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.loop();  // Set it to continous playback (looping)
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }
    }
    
    /**
     * This method is called upon entering portal after the target score has been reached.
     * It will load up the next level and will save and record the previous level score and lives allowing
     * it to carry on to the next level. If the player reaches the last level of the game it will then check if they have achieved
     * a highscore and then exit the game.
     */
    public void nextLevel(){
        world.stop();
        lastLives = getPlayer().getLives();
        lastScore = getPlayer().getScore();
        levelSelect++;
        if(levelSelect == level.length){
            HighScore hs = new HighScore();
            hs.checkScore(getPlayer().getScore());
            System.exit(0);
        }else{
            musicSettings();
            world = level[levelSelect];
            world.setTarget(lastScore);
            world.populate(this);
            cp.setWorld(world);
            controller.setBody(world.getPlayer(),world);
            view.setBody(world.getPlayer());
            view.sW(world); //used to get targetlevel
            view.setWorld(world);
            world.addStepListener(new CameraFollow(view, world.getPlayer()));
            view.newBG();
            getPlayer().setLives(lastLives);
            getPlayer().setScore(lastScore);
            world.start();
            //JFrame debugView = new DebugViewer(world, 500, 500);
        }
    }
    
    /**
     * This method is called when the user presses restart button on the control panel.
     * The method stops the simulation then removes all static and dynamic bodies and pauses any timers.
     * It will then repopulate the world with new instances of the bodies.
     * The players score will be reset to 0 and lives will be refreshed, as result the target score will be set 
     * to 0. As TargetScore is calculate from previous playerscore + the level targetscore.
     */
    public void restartLevel(){
        world.stop();
        
        List<DynamicBody> list = ((World)world).getDynamicBodies();
        List<StaticBody> list2 = ((World)world).getStaticBodies();
        
        for (DynamicBody s : list) {
            s.destroy();
              if (s instanceof TimerEnemy) {
                TimerEnemy d = (TimerEnemy) s;
                d.pauseTimer();
              }
        }
        for (StaticBody s : list2) {
            s.destroy();
        }

        world = level[levelSelect];
        world.setTarget(0);
        world.populate(this);
        controller.setBody(world.getPlayer(),world);
        view.setBody(world.getPlayer());
        view.setWorld(world);
        world.addStepListener(new CameraFollow(view, world.getPlayer()));
        view.newBG();
        world.start();
    }
    /**
     * Returns what level the player is currently on.
     * @return int
     */
    public int returnLevel(){
        return levelSelect;
    }
    /**
     * The main method used to start the game.
     * @param args String[]
     */
    public static void main(String[] args) {
        new MyGame();
    }
    
}
