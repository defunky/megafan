/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;

/**
 * This class is used to create a no friction panel thats applied to side of
 * platforms to prevent player from sticking onto the side.
 * @author Abdalrahmane
 */
public class NoFriction extends StaticBody {
    private final float height;

    /**
     * Creates a SolidFixture with friction of 0. The height is adjustable.
     * @param w the current level
     * @param height the height desired.
     */
    public NoFriction(World w, float height) {
        super(w);
        this.height = height;
        Shape shape = new BoxShape(0, height);
        SolidFixture panel = new SolidFixture(this,shape);
        panel.setFriction(0);
    }

    
}
