/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import city.cs.engine.World;
import org.jbox2d.common.Vec2;

/**
 * This class uses the NoFriction class to create two panels at a given x and y position.
 * This allows quick and easy way to add no stick walls to platforms and walls.
 * @author Abdalrahmane
 */
public class NoFrictionPanel extends StaticBody {
    private NoFriction left,right;
    private float width,height,x,y,x1,x2;
    /**
     * Creates a pair of NoFrictionPanels at a desired location.
     * @param w the current world
     * @param width width of panel
     * @param height height of the panel
     * @param x the x coord to place the panel
     * @param y the y coord to place the panel
     */
    public NoFrictionPanel(World w, float width, float height, float x, float y) {
        super(w);
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        
        x1 = x - width;
        x2 = x + width;
        left = new NoFriction(w,height - 0.1f);
        left.setPosition(new Vec2(x1,y));
        right = new NoFriction(w,height - 0.1f);
        right.setPosition(new Vec2(x2,y));
    }
    
}
