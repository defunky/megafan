/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A Collision Listener for the live pickup.
 * @author Abdalrahmane
 */
public class Pickup implements CollisionListener {
    private Player player;
    private SoundClip gameMusic;
    /**
     * gets the current player instance to correctly increment the lives
     * @param player current instance of the player
     */
    public Pickup(Player player) {
        this.player = player;
    }

    /**
     * When the player collides with the pickup, it will increase the current lives
     * destroy the pickup with song to indicate that pickup has been collected.
     * @param e CollisionEvent
     */
    @Override
    public void collide(CollisionEvent e) {
        if (e.getOtherBody() == player) {
            player.incrementLives();
            e.getReportingBody().destroy();
            music();
        }
    }
    
    public void music(){
        try {
            gameMusic = new SoundClip("data/music/livepickup.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  

    }
}
