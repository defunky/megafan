package mygame;
import city.cs.engine.*;
import java.awt.event.KeyEvent;
import org.jbox2d.common.Vec2;
import java.io.IOException;
import java.util.List;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * The playable character that the user is able to control
 * @author Abdalrahmane
 */
public class Player extends Walker{
    private static BodyImage bodyImage;
    private static Shape playerShape;
    private static SoundClip gameMusic;
    private static final float IMG_SIZE = 2.5f;
    private int lives = 3;
    private int score = 0;
    private World world;
    
    /**
     * Creates an instance of the player, using SolidFixture and sets an idle body image.
     * the gravity is changed slightly to allow more a fluid control.
     * @param world The current level 
     */
    public Player(World world) {
        super(world);
        playerShape = new BoxShape(0.75f,1.3f);
        SolidFixture fixture = new SolidFixture(this, playerShape);
        fixture.setFriction(10);
        bodyImage = new BodyImage("data/player/playeridle.gif",2.5f);
        addImage(bodyImage);
        setGravityScale(1.5f);
        //setPosition(new Vec2(-8,-10));
        this.world = world;
    }
    
    /**
     * Returns the amount of lives the player has
     * @return lives
     */
    public int getLives(){
        return lives;
    }
    /**
     * Returns the players score
     * @return score
     */
    public int getScore(){
        return score;
    }
    /**
     * Sets the players score
     * @param score amount of score wanted
     */
    public void setScore(int score){
        this.score = score;
    }
    /**
     * Sets the players lives
     * @param lives amount of lives wanted
     */
    public void setLives(int lives){
        this.lives = lives;
    }
    /**
     * Increases the player score by one, used when enemy is killed.
     */
    public void increaseScore(){
        score++;
    }
    /**
     * Increases lives by one - called when LivePickup is collected.
     */
    public void incrementLives(){
        lives++;
        System.out.println("Lives increased: "+lives);
    }   
    /**
     * Changes the animation of the player  depending on what action the player
     * is currently performing
     * @param code The button last pressed by the user
     * @param jump If the player has jumped
     * @param release if the player has released the button
     */
    public void animation(int code, boolean jump, boolean release){
        removeImage(bodyImage);
        //System.out.println(code);
        if(code == KeyEvent.VK_LEFT && !jump && !release){
            bodyImage = new BodyImage("data/player/playerwalkleft.gif",IMG_SIZE);
        }else if(code == KeyEvent.VK_LEFT && jump && !release){
            bodyImage = new BodyImage("data/player/playerjumpleft.gif",IMG_SIZE +0.5f);
        }else if(code == KeyEvent.VK_RIGHT && !jump && !release){
            bodyImage = new BodyImage("data/player/playerwalk.gif",IMG_SIZE);
        }else if(code == KeyEvent.VK_RIGHT && jump && !release){
            bodyImage = new BodyImage("data/player/playerjump.gif",IMG_SIZE +0.5f);
        }else if(code == KeyEvent.VK_SPACE && !release){
            bodyImage = new BodyImage("data/player/playerjump.gif",IMG_SIZE +0.5f);
        }else if(code == KeyEvent.VK_LEFT && !jump && release){
            bodyImage = new BodyImage("data/player/playeridleleft.gif",IMG_SIZE);
        }else if(code == KeyEvent.VK_RIGHT && !jump && release){
            bodyImage = new BodyImage("data/player/playeridle.gif",IMG_SIZE);
        }
        addImage(bodyImage);
    }
    /**
     * This method is called when the player is struck by enemy projectile, or touches
     * the enemy. It will decrease lives by one, if there are no more lives left the player 
     * is destroy and game over screen occurs. The player score is then checked to see if
     * they have achieved highscore.
     */
    public void hurt(){  
        lives--;
         if(lives < 1){
            destroy();
            List<DynamicBody> list = world.getDynamicBodies();
            for (DynamicBody bodies : list) {
              if (bodies instanceof TimerEnemy) {
                TimerEnemy te = (TimerEnemy) bodies;
                    te.pauseTimer();
              }
            }
            HighScore hs = new HighScore();
            hs.checkScore(getScore());
        }
        try {
            gameMusic = new SoundClip("data/music/hit.wav");   // Open an audio input stream
            gameMusic.setVolume(0.5f);
            gameMusic.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
    
}
