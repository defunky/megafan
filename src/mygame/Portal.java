/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.BodyImage;
import city.cs.engine.BoxShape;
import city.cs.engine.Shape;
import city.cs.engine.StaticBody;
import city.cs.engine.World;
import org.jbox2d.common.Vec2;

/**
 * A class used to a create a portal to allow the player to proceed to the next
 * level.
 * @author Abdalrahmane
 */
public class Portal extends StaticBody {
    private static final Shape shape = new BoxShape(0.8f, 1.75f);
    /**
     * Places a portal in the current level and gives it portal body image
     * @param w current level
     */
    public Portal(World w) {
        super(w, shape);
        addImage(new BodyImage("data/misc/portal.gif",3.5f));
    }
    
}
