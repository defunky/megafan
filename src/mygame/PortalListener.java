/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.CollisionEvent;
import city.cs.engine.CollisionListener;

/**
 * A collision listener used in conjuction with Portal to allow the player to move
 * to the next level
 * @author Abdalrahmane
 */
public class PortalListener implements CollisionListener {
    private MyGame game;
    /**
     * 
     * @param game current instance of the game
     */
    public PortalListener(MyGame game){
        this.game = game;
    }
    /**
     * Checks whether the player has collided with the portal, if the player has reached
     * target score they will be transported to the next level otherwise nothing will happen.
     * @param e CollisionEvent
     */
    @Override
    public void collide(CollisionEvent e) {
        if (e.getOtherBody() == game.getPlayer() && game.isCompleted()){
            game.nextLevel();
        }
    }
    
}
