/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.*;
import org.jbox2d.common.Vec2;

/**
 * A enemy used in level 2, has a short range distance but extremely fast, and only
 * requires to be hit twice.
 * @author Abdalrahmane
 */
public class ShellEnemy extends Enemy {
    private static final int WALKING_SPEED = 5;
    private static final int RANGE_DISTANCE = 5;
    private static final int JUMPING_SPEED = 0;
    private static final int HEALTH = 2;
    private SolidFixture fixture;
    private BodyImage bodyImage;
    
    /**
     * ShellEnemy is created with a solid fixture has a high fixture density.
     * The constructor also sets the Health, Walking Speed, Jump Speed and Range Distance.
     * <p>Walking Speed = 5 </p>
     * <p>Jumping Speed = 0</p>
     * <p>Health = 2</p>
     * <p>Range Distance = 5</p>
     * @param world the current level
     * @param player the current world.
     */
    public ShellEnemy(World world, Player player) {
        super(world, player);
        setHealth(HEALTH);
        setWalkingSpeed(WALKING_SPEED);
        setJumpSpeed(JUMPING_SPEED);
        setRangeDistance(RANGE_DISTANCE);
        BoxShape enemyShape = new BoxShape(0.75f,0.75f);
        fixture = new SolidFixture(this, enemyShape);
        fixture.setDensity(20);
        bodyImage = new BodyImage("data/shellenemy/idle.gif",1.2f);
        addImage(bodyImage);
    }
    
    @Override
    public SolidFixture getFixture() {
        return fixture;
    }


    @Override
    public void startPosition(Vec2 startPos) {
        setPosition(startPos);
    }

    @Override
    public int getWalkingSpeed() {
        return WALKING_SPEED;
    }

    @Override
    public int getRangeDistance() {
        return RANGE_DISTANCE;
    }

    @Override
    public int getJumpSpeed() {
        return JUMPING_SPEED;
    }

    @Override
    public void animate(String currentState) {
        removeImage(bodyImage);
        if(currentState.equals("right")){
            bodyImage = new BodyImage("data/shellenemy/walkright.gif",1.2f);
        }else if(currentState.equals("left")){
            bodyImage = new BodyImage("data/shellenemy/walk.gif",1.2f);
        }else if(currentState.equals("idle")){
            bodyImage = new BodyImage("data/shellenemy/idle.gif",1.2f);
        }
        addImage(bodyImage);
    }
    
}
