/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import city.cs.engine.SoundClip;
import city.cs.engine.World;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Timer;

/**
 * An abstract class that inherits Enemy Class. Instances of TimerEnemy uses Swing Timer 
 * to fire projectiles at the player at specific time intervals.
 * @author Abdalrahmane
 */
public abstract class TimerEnemy extends Enemy {
    private static SoundClip sc;
    /**
     * Constructor allowing creation of the a enemy that inherits TimerEnemy.
     * @param world the current level
     * @param player the current player, that user is controlling
     */
    public TimerEnemy(World world, Player player) {
        super(world,player);
    }
    
    /**
     * Pauses the timer preventing from the enemy from firing projectiles anymore.
     */
    public abstract void pauseTimer();
    /**
     * Starts the timer again, allowing enemy to fire projectiles.
     */
    public abstract void startTimer();
    
    /**
     * This method is called when the enemy has been hit by a projectile from the player.
     */
    public void hitSound(){
            try {
            sc = new SoundClip("data/music/hit.wav");   // Open an audio input stream
            sc.setVolume(0.5f);
            sc.play();
        } catch (UnsupportedAudioFileException|IOException|LineUnavailableException e) {
            System.out.println(e);
        }  
    }
    
    
}
